# Gitlab Tutorials für Akademische Lehre

Dies ist ein Brainstorming, was ein Tutorial für Gitlab für Texte/OER/Akademische Lehre mitbringen muss, damit viele es nutzen.

---

## Generelle Anforderungen
- Link/Tutorial direkt vom OERSI aus
- einfache, gut zu findende Übersicht über Vorlagen
- Muss schnell & ohne viel (doppelten) Aufwand gehen  
&rarr; Tutorial/selber Ausprobieren dauert höchstens 5 Minuten  
&rarr; am besten nicht mehr als drei Klicks/Views bis zur ersten Version, kein langer Tutorial-Kurs
- ggf. Einbindung in Institutswebseite / StudIP
- ggf. simple Benutzeroberfläche mit Infografiken zur Auswahl der Vorlage
- ggf. autom. Umwandlung docx/ippx/etc.
- ggf. mehrere Sprachen (nicht jeder kann gut Englisch, in Gitlab ist (fast) alles auf Englisch, allein die OERSI-Issue-Maske)

### Use Case 1: Folien/Buch/etc. direkt hochladen
- einfacher Upload der VL-Folien, ggf. direkt in einer Metadaten-Maske
- vermutlich pdf  
&rarr; Maske zum erstellen von OER, legt Repo an, erstellt Metadaten für Repo, lädt Materialien direkt hoch, direkte Weiterleitung zum Repo

### Use Case 2: Texte einstellen
- Nutzung einer Kursvorlage
- Einfügen der Texte (vermutlich ohne Formatierung)
- docx/latex &rarr; markdown via pandoc ? &rarr; autom. parsen

### Use Case 3: Ganzen Kurs selber erstellen
- ggf. Nutzung einer Kursvorlage
- oder Import von z.B. Moodle?

## Aufbau eines Tutorials
1. Gitlab-Account erstellen (die meisten werden keinen haben)
2. Neues Repository erstellen / Kursvorlage klonen/importieren
3. Metadaten eintragen & hochladen (Formular/geklonte Datei)
4. Dateien hochladen / erstellen
5. In OERSI eintragen: Issue erstellen / kontaktieren

Schritt 2-4 ggf. in bzw. mit der Metadaten-Maske automatisieren!

Schritt 5 ggf. auch automatisieren, z.B. durch Git-interne Flags, oder vorgefertigte E-Mail

---

## Möglichkeiten zur Vereinfachung der OER-Erstellung
* Simple Desktop-Anwendung, die direkt alles für einen macht (git repo anlegen, kursvorlage pullen, Metadaten-Generator, ... )
* Langsames, aber einfaches und kurzes Schritt-für-Schritt-Video-Tutorial

## Allgemeine Gedanken und Bedenken
- Markdown ist zu unstrukturiert, um Abschlussarbeiten, Folien, etc. mit hohem Anspruch damit erstellen zu können  
&rarr; visueller Anspruch ist hoch, und auch wichtig zum Lernen (gute Formatierung und räumlicher Zusammenhang Bilder+Texte) bzw. sehr wichtig für die Note  
&rarr; die Erstellung wird also sehr wahrscheinlich außerhalb von Gitlab passieren  
&rarr; damit ist der Aufwand doppelt, wenn man die Dokumente dann noch in offene Formate wie Markdown überführen will

- Markdown in Gitlab ist noch mal etwas anders bzw. erweitert
&rarr; das macht  
a) das Googlen schwer (Nicht-Programmierer:innen sind auch nicht so versiert im Formulieren der richtigen Google-Anfragen und Filtern wie Programmierer:innen) und  
b) uneinheitlich, funktioniert dann ggf. lokal nicht mehr

- also vielleicht lieber ein anderes Format in Markdown transformieren?  
&rarr; dann geht aber evtl. viel verloren

- Git ist für Nicht-Programmierer:innen vermutlich überfordernd, was sehr demotivierend kann kann. Git hat auch sehr viel mehr Features als man wirklich braucht, was ebenfalls verwirren kann. Eine offene Cloud mit Metadaten-Generator würde z.B. völlig ausreichen (die muss dann aber natürlich von jemandem administriert und auf Qualität überprüft werden)  
&rarr; vielleicht wäre eine Art "OER-Cloud" am besten geeignet, bei der man sich nur ein Verzeichnis anlegen bzw. nur etwas hochladen kann, sofern man die dazugehörige aufploppende, nicht wegklickbare Metadaten-Maske ausfüllt. Bei der Maske kann es die Möglichkeit zur automatischen Füllung aller sich nicht oft ändernden Felder geben (Name, Institution, Sprache, etc.)  
&rarr; Gitlab für Lehre/OER/Texte evtl. nur für technikaffine Menschen bewerben, vor allem die, die sich sowieso schon mit Git auskennen  
&rarr; Tutorials davon abhängig machen, wer man ist, was für Skills man schon mitbringt, wie viel Zeit & Motivation man hat

- Metadaten-Generator updaten und ggf. anderswo einbauen