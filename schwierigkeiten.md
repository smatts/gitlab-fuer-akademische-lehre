# Mögliche Schwierigkeiten

Dies ist eine kurze Zusammenfassung von Schwierigkeiten, die mir beim Durchlaufen des gesamten Prozesses aufgefallen sind und Hürden für die Erstellung von OER in GitLab sein könnten.

## Erstellen eines GitLab-Accounts:
* Der Account-Erstellen-Link ist etwas versteckt, man muss erst auf *Sign In* klicken und dann unten auf *Sign Up*.  
&rarr; Lösung: *Sign Up* Link direkt im Tutorial verlinken.
* Während des Einrichtens wird man direkt dazu aufgefordert, seine Rolle zu wählen, zu wählen ob man im Team oder alleine arbeitet, usw., und man erstellt sofort sein erstes eigenes Projekt, wenn man es nicht importieren will. Dabei wird eine Gruppe erstellt. Bei mir waren beide direkt automatisch auf *private* gestellt. Das Projekt kann dann auch nur auf *public* umgeschaltet werden, wenn man vorher die Gruppe *public* macht.
&rarr; Lösung: "Join an existing project" anstatt "Create a new project" umgeht all dies ganz einfach. Alle Felder können in einem Tutorial angegeben werden.
* Beim Erstellen eines neuen Projektes muss man die Gruppe auswählen. Standardmäßig wird dann die vorher erstellte, ggf. *private* Gruppe ausgewählt.  
&rarr; Lösung: Erklären, dass man die Gruppe *public* schaltet, oder auf eigenen Usernamen zeigen lassen (aufpassen, dass eigener Username und Gruppe nicht gleich/fast gleich heißen). Am besten aber einfach wie oben beschrieben "Join an existing project" wählen, dann taucht das Problem nicht auf.

## GitLab-Oberfläche
### Allgemein
* Viele Felder, die erstmal Verwirrung stiften könnten
### IDE
* Erstmal Finden des Buttons
* Sofern man noch nie eine benutzt hat, generell 
* Kein automatisches Speichern, man muss erst committen und wissen, was das ist, was "staged" bedeutet, usw.  
&rarr; Lösung: nicht die IDE verwenden, Dateien einzeln im Editor verändern

## Pipeline
* Ich kann als **neuer**, nicht zahlender Nutzer die Pipeline nicht mehr durch shared runners durchführen lassen:

![](img/validation_required.png)

* Man muss entweder eine Kreditkarte angeben (wenn man denn eine hat) oder seinen eigenen Runner laufen lassen. Das wird vermutlich niemand machen.  
&rarr; 1. mögliche Lösung: Kurse von außerhalb generieren, z.B. von TIB-Seite aus (das macht zumindest das Erstellen das Repositories einfacher, wenn man sich nicht auch noch um die Pipeline kümmern muss)  
&rarr; 2. mögliche Lösung: nicht gitlab.com benutzen